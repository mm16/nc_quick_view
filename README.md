# nc_quick_view

NetCDF viewer written in Python.

## Features

* User interface for easy usage
* Simple statistics for variables
* Show attributes
* Line plotting for variables data

## Installation

    # git clone
    python3 -m venv venv  # optional: create venv
    source venv/bin/activate  # optional: activate venv on Linux
    pip install -r requirements.txt

## Start nc_quick_view

    python nc_quick_view/main_gui.py

## Configuration

All config options are in files in *nc_quick_view/configs*, mostly in file 
*nc_quick_view/configs/gui.py*.