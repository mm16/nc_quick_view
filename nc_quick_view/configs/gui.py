GUI_DEFAULTS = {
    "QLineEdit": {
        "self.file_inputs.nc_file_name": "",
    }
}
GUI_GEOMETRY = (300, 200, 1300, 800)
GUI_SETTINGS_FILE = ".gui_settings"
GUI_WINDOW_TITLE = "NetCDF Quick View"
PLOT_INDEX_LIMITS = (0, 99999)
