import numpy as np


def rearrange_dimensions(arr: np.ndarray, x_dim: int, y_dim: int) -> np.ndarray:
    """
    Parameters
    ----------
    x_dim : Index of dimension of x-coordinate (eg the time coordinate)
    y_dim : Index of dimension of per-line plotting

    This function does
    - remove unneeded dimensions
    - transpose result if needed, so that one line (plotting) loop is over second
      dimension
    - return 2D array

    """
    out_arr = arr.copy()
    indices_to_keep = set([x_dim, y_dim])
    dims_to_reduce = set(np.arange(arr.ndim)) - indices_to_keep
    for dim_to_reduce in dims_to_reduce:
        # only take the first 0. array along the axis "not of interest"
        # we couldn't plot them anyways, we take the first for no reason
        out_arr = np.take(out_arr, [0], axis=dim_to_reduce)

    # remove dims
    out_arr = out_arr.squeeze((tuple(dims_to_reduce)))

    if x_dim > y_dim:
        out_arr = out_arr.transpose()

    return out_arr
