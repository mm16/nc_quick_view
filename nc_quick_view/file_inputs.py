import logging
import pathlib
from typing import Callable

from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QGroupBox
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QWidget


LOGGER = logging.getLogger(__name__)


class GroupBox(QGroupBox):
    def __init__(self, load_file_function: Callable, write_gui_settings: Callable):
        super().__init__("File inputs")
        self.load_file_function = load_file_function

        vbox = QVBoxLayout()

        self.nc_file_name = QLineEdit()
        self.nc_file_name.textChanged.connect(lambda: write_gui_settings())
        bt_get_file = QPushButton("Get netcdf file")
        # the lambda args are needed to have correct args passed to get_file (otherwise
        # always the last lineedit is used)
        bt_get_file.pressed.connect(lambda le=self.nc_file_name: self._get_file(le))
        bt_reload = QPushButton("Reload")
        bt_reload.pressed.connect(
            lambda: self.load_file_function(self.nc_file_name.text())
        )

        hbox_per_file = QHBoxLayout()
        hbox_per_file.addWidget(bt_get_file)
        hbox_per_file.addWidget(self.nc_file_name)
        hbox_per_file.addWidget(bt_reload)

        horizontal_widget = QWidget()
        horizontal_widget.setLayout(hbox_per_file)
        vbox.addWidget(horizontal_widget)

        self.setLayout(vbox)

    def _get_file(self, lineedit: QLineEdit):
        initial_directory = str(pathlib.Path(lineedit.text()).parent)
        filename = QFileDialog.getOpenFileName(
            self, "Select a netCDF file", initial_directory, ""
        )

        if filename[0]:
            lineedit.setText(filename[0])
            self.load_file_function(filename[0])
