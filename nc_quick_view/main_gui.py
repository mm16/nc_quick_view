import copy
import logging
import logging.config
import sys
from collections import namedtuple

import netCDF4
import numpy as np
from PyQt5.QtWidgets import QApplication, QHBoxLayout, QMainWindow, QVBoxLayout, QWidget
import toml

import file_inputs
import tables
import plots
import configs.gui
import configs.logs
from data_manipulation import rearrange_dimensions


logging.config.dictConfig(configs.logs.LOGGING_CONFIG)
LOGGER = logging.getLogger(__name__)

# "function" to create gui settings file (ie get values of the gui widgets)
SETTINGS_TO_CALL = {"QLineEdit": "{key:s}.setText('{value:s}')"}

# "function" to set the widget values when loading the gui settings
SETTINGS_TO_CALL_GET = {"QLineEdit": "{key:s}.text()"}
DimensionToPlot = namedtuple("DimensionToPlot", ["x", "y"])


def check_if_plottable(
    nc_dataset: netCDF4.Dataset,
    nc_variable: netCDF4.Variable,
    dim_to_plot: DimensionToPlot,
) -> bool:
    if not dim_to_plot.x:
        LOGGER.debug("No x var to plot selected (probably automatic trigger callback")
        return False

    # try if values can be plotted
    try:
        _ = nc_dataset.variables[dim_to_plot.y]
        _ = [float(x) for x in nc_dataset.variables[dim_to_plot.x][:]]
    except ValueError:
        LOGGER.debug("Values are not float-able (not able to plot). Skip plotting")
        return False
    except KeyError as e:
        LOGGER.info(f"Key error: {str(e)}. Skip plotting")
        return False

    data = nc_variable[:]

    if data.ndim > 1:
        if not dim_to_plot.y:
            LOGGER.debug(
                "No y dimension for plotting chosen. Probably automatically"
                "triggered callback. Returning"
            )
            return False
        if dim_to_plot.x == dim_to_plot.y:
            LOGGER.debug(
                f"Equal dimensions for x and y selected ('{dim_to_plot.x}')."
                f"This is not allowed for plotting 2D variables. Returning"
            )
            return False

        try:
            wanted_index = nc_variable.dimensions.index(dim_to_plot.y)
            wanted_index_x = nc_variable.dimensions.index(dim_to_plot.x)
        except ValueError:
            LOGGER.debug(
                f"One of desired variables {dim_to_plot} does not exist. Probably not "
                "yet set, but this callback already triggered"
            )
            return False

    return True


if __name__ == "__main__":

    class Window(QMainWindow):
        def __init__(self):
            super().__init__()
            LOGGER.debug("Initializing GUI")

            self.nc_dataset = None
            self.current_gui_settings = copy.deepcopy(configs.gui.GUI_DEFAULTS)

            self.setGeometry(*configs.gui.GUI_GEOMETRY)
            self.setWindowTitle(configs.gui.GUI_WINDOW_TITLE)
            central_widget = QWidget()
            central_layout = QVBoxLayout()

            self.plot = plots.Plot(self.plot_variable)
            self.content_tables = tables.ContentTables(self.fill_x_y_plot_dropdown)
            self.file_inputs = file_inputs.GroupBox(
                self.read_nc_file, self.write_gui_settings
            )
            horizontal_widget_below = QWidget()
            horizontal_layout_below = QHBoxLayout()
            horizontal_layout_below.addWidget(self.content_tables)
            horizontal_layout_below.addWidget(self.plot)
            horizontal_widget_below.setLayout(horizontal_layout_below)

            central_layout.addWidget(self.file_inputs)
            # central_layout.addWidget(tables.content_tables())
            central_layout.addWidget(horizontal_widget_below)
            central_widget.setLayout(central_layout)

            self.setCentralWidget(central_widget)

            try:
                gui_settings = toml.load(configs.gui.GUI_SETTINGS_FILE)
            except FileNotFoundError:
                gui_settings = {}
            self.load_gui_settings(gui_settings, defaults=configs.gui.GUI_DEFAULTS)

        def fill_x_y_plot_dropdown(self):
            data: netCDF4.Variable = self._get_currently_selected_variable()
            if not data:
                # no cell selected (I think) - can happen when we load another file
                return
            self.plot.fill_data(data)
            self.plot_variable()

        def read_nc_file(self, fn: str):
            LOGGER.debug(f"Reading {fn}")
            self.nc_dataset = netCDF4.Dataset(fn, mode="r")
            self.fill_content_tables()

        def fill_content_tables(self):
            if not self.nc_dataset:
                LOGGER.warning("netCDF dataset is not loaded yet, please read nc first")
                return
            self.content_tables.fill_data(self.nc_dataset)

        def write_gui_settings(self):
            for widget_type, vals in self.current_gui_settings.items():
                for widget_name, current_val in vals.items():
                    call = SETTINGS_TO_CALL_GET[widget_type].format(key=widget_name)
                    self.current_gui_settings[widget_type][widget_name] = eval(call)
            with open(configs.gui.GUI_SETTINGS_FILE, "w") as f:
                toml.dump(self.current_gui_settings, f)

        def load_gui_defaults(self):
            self.load_gui_settings(configs.gui.GUI_DEFAULTS)

        def load_gui_settings(self, settings: dict, defaults: dict = None):
            LOGGER.debug("Loading gui settings")
            # make empty settings (if gui settings file not exist) work -> add defaults
            dict_with_defaults = {**defaults, **settings}
            for widget_type, settings_to_call in SETTINGS_TO_CALL.items():
                for k, v in dict_with_defaults[widget_type].items():
                    call = settings_to_call.format(key=k, value=v)
                    try:
                        eval(call)
                    except Exception as e:
                        LOGGER.debug(
                            f"Failing to assign gui setting {k} on {v} with {call}. "
                            f"Try defaults? {'yes' if defaults else 'no'}"
                        )
                        if defaults:
                            try:
                                call = settings_to_call.format(
                                    k, defaults[widget_type][k]
                                )
                                eval(call)
                            except Exception as e:
                                LOGGER.warning(f"Failing to assign defaults!")

        def _get_currently_selected_variable(self) -> netCDF4.Variable:
            try:
                row: int = self.content_tables.table_vars.selectedIndexes()[0].row()
            except IndexError:
                return None
            var_to_plot: str = self.content_tables.variablenames_in_table[row]
            return self.nc_dataset.variables[var_to_plot]

        def plot_variable(self):
            self.plot.plot_widget.clear()
            dim_to_plot = DimensionToPlot(
                self.plot.x_var.currentText(), self.plot.y_var.currentText()
            )
            var = self._get_currently_selected_variable()

            if not check_if_plottable(self.nc_dataset, var, dim_to_plot):
                return

            # get variable to plot
            LOGGER.debug(f"Start plotting {var.name}")

            x_values = self.nc_dataset.variables[dim_to_plot.x][:]

            data: np.ndarray = var[:].filled(np.nan)

            if data.ndim > 1:
                wanted_index = var.dimensions.index(dim_to_plot.y)
                wanted_index_x = var.dimensions.index(dim_to_plot.x)
                line_names = self.nc_dataset.variables[dim_to_plot.y][:]
                line_names = [str(x) for x in line_names]

                data = rearrange_dimensions(data, wanted_index_x, wanted_index)
                loop_indices = plots.get_indices_to_plt(
                    data, configs.gui.PLOT_INDEX_LIMITS
                )
                LOGGER.debug(
                    f"Looping over indices: {loop_indices} (last ind is "
                    "one larger than 'number')"
                )
                number_of_lines: int = loop_indices[1] - loop_indices[0] + 1
                for line_ind in np.arange(loop_indices[0], loop_indices[1]):
                    y = data[:, line_ind]
                    try:
                        self.plot.plot_widget.plot(
                            x_values,
                            y.flatten(),
                            pen=(line_ind, number_of_lines),
                            name=line_names[line_ind],
                        )
                    except Exception as e:
                        LOGGER.error(f"Error when plotting: y={y}: {str(e)}")
            else:
                try:
                    self.plot.plot_widget.plot(x_values, data, name=var.name)
                except Exception as e:
                    LOGGER.error(f"Error when plotting 1D-data: y={data}. {str(e)}")

    app = QApplication(sys.argv)
    window = Window()
    window.show()
    sys.exit(app.exec_())
