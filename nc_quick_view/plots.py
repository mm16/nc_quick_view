import logging

import netCDF4
import numpy as np
import pyqtgraph as pg
from typing import Callable

from PyQt5.QtWidgets import QComboBox
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QWidget


class Plot(QWidget):
    def __init__(self, plot_variable: Callable):
        super().__init__()

        vbox = QVBoxLayout()

        self.x_var = QComboBox()
        self.x_var.activated.connect(lambda: plot_variable())
        self.y_var = QComboBox()
        self.y_var.activated.connect(lambda: plot_variable())

        hbox_layout_x_y_variable = QHBoxLayout()
        hbox_layout_x_y_variable.addWidget(self.x_var)
        hbox_layout_x_y_variable.addWidget(self.y_var)
        hbox_widget = QWidget()
        hbox_widget.setLayout(hbox_layout_x_y_variable)

        self.plot_widget = pg.PlotWidget()
        self.plot_widget.setBackground("w")
        self.plot_widget.addLegend()

        vbox.addWidget(self.plot_widget)
        vbox.addWidget(hbox_widget)
        self.setLayout(vbox)

    def fill_data(self, data: netCDF4.Variable):
        dims = data.dimensions
        self.x_var.clear()
        self.y_var.clear()

        self.x_var.addItems(dims)
        self.y_var.addItems(dims)

        self.x_var.setCurrentIndex(0)
        if len(dims) > 1:
            self.y_var.setCurrentIndex(1)


def get_indices_to_plt(arr: np.ndarray, plot_index_limits: tuple[int]) -> tuple[int]:
    loop_start = min([arr.shape[1], plot_index_limits[0]])
    loop_end = min([arr.shape[1], plot_index_limits[1]])
    return (loop_start, loop_end)
