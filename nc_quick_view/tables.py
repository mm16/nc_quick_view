import logging
from typing import Callable

import netCDF4
import numpy as np
from PyQt5.QtWidgets import QGroupBox
from PyQt5.QtWidgets import QTableWidget
from PyQt5.QtWidgets import QTableWidgetItem
from PyQt5.QtWidgets import QVBoxLayout

VARIABLES_HEADER = (
    "name/dimensions",
    "type",
    "shape",
    "data",
    "min",
    "max",
    "mean",
    "median",
)
ATTRIBUTES_HEADER = ("attribute", "value")
DIMENSIONS_HEADER = ("name", "unlimited?", "size")

LOGGER = logging.getLogger(__name__)


def to_variable_table_content(variable: netCDF4.Variable, col_index: int) -> str:
    """col_index matches VARIABLES_HEADER"""
    if col_index == 0:
        return f"{variable.name}({', '.join(variable.dimensions)})"
    if col_index == 1:
        return f"{variable.dtype}"
    if col_index == 2:
        return f"{variable.shape}"
    if col_index == 3:
        if isinstance(variable[:], np.ndarray):
            return str(variable[:])
        return f"{str(variable[:].filled(np.nan))}"
    if col_index in (4, 5, 6, 7):
        func = {4: np.nanmin, 5: np.nanmax, 6: np.nanmean, 7: np.nanmedian}[col_index]
        try:
            return str(func(variable[:].filled(np.nan)))
        except Exception:
            return "?"
    raise RuntimeError(
        f"Nothing implemented for attribute number {col_index}. Expecting an index of"
        f"{VARIABLES_HEADER}"
    )


class ContentTables(QGroupBox):
    def __init__(self, fill_x_y_plot_dropdown: Callable):
        super().__init__("File variables, attributes")

        self.table_vars = QTableWidget()
        self.table_vars.itemSelectionChanged.connect(lambda: fill_x_y_plot_dropdown())
        self.table_vars.setRowCount(4)
        self.table_vars.setColumnCount(len(VARIABLES_HEADER))
        self.table_vars.setHorizontalHeaderLabels(VARIABLES_HEADER)
        # store the order of the rows in the table (might be inconsistent due to dict)
        self.variablenames_in_table = None

        self.table_attr = QTableWidget()
        self.table_attr.setRowCount(4)
        self.table_attr.setColumnCount(len(ATTRIBUTES_HEADER))
        self.table_attr.setHorizontalHeaderLabels(ATTRIBUTES_HEADER)

        self.table_dims = QTableWidget()
        self.table_dims.setRowCount(3)
        self.table_dims.setColumnCount(len(DIMENSIONS_HEADER))
        self.table_dims.setHorizontalHeaderLabels(DIMENSIONS_HEADER)

        vbox = QVBoxLayout()
        vbox.addWidget(self.table_vars)
        vbox.addWidget(self.table_attr)
        vbox.addWidget(self.table_dims)
        self.setLayout(vbox)

    def fill_data(self, dataset: netCDF4.Dataset):
        self._fill_data_variables(dataset)
        self._fill_data_attributes(dataset)
        self._fill_data_dimensions(dataset)

    def _fill_data_attributes(self, dataset: netCDF4.Dataset):
        self.table_attr.clearContents()
        attributes = dataset.ncattrs()
        self.table_attr.setRowCount(len(attributes))
        for row_ind, ncattr in enumerate(attributes):
            try:
                attr_val = str(getattr(dataset, ncattr))
            except Exception:
                attr_val = "?"
            self.table_attr.setItem(row_ind, 0, QTableWidgetItem(ncattr))
            self.table_attr.setItem(row_ind, 1, QTableWidgetItem(attr_val))

    def _fill_data_variables(self, dataset: netCDF4.Dataset):
        self.table_vars.clearContents()
        self.table_vars.setRowCount(len(dataset.variables))
        variables_in_table = []
        for row, (var_name, v) in enumerate(dataset.variables.items()):
            variables_in_table.append(var_name)
            for col_index in range(len(VARIABLES_HEADER)):
                try:
                    cell_content = to_variable_table_content(v, col_index=col_index)
                except Exception:
                    cell_content = "?"
                self.table_vars.setItem(row, col_index, QTableWidgetItem(cell_content))
        self.variablenames_in_table = variables_in_table

    def _fill_data_dimensions(self, dataset: netCDF4.Dataset):
        self.table_dims.clearContents()
        self.table_dims.setRowCount(len(dataset.dimensions))
        for row, dim in enumerate(dataset.dimensions.values()):
            try:
                cell_values = (
                    dim.name,
                    "yes" if dim.isunlimited() else "no",
                    str(dim.size),
                )
            except Exception:
                LOGGER.warning(f"Error when adding {dim} to dimensions table")
                cell_values = ("?", "?", "?")
            for col, cell_value in enumerate(cell_values):
                self.table_dims.setItem(row, col, QTableWidgetItem(cell_value))
