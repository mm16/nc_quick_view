import numpy as np

from data_manipulation import rearrange_dimensions


ARR = np.array(
    [
        [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]],
        [[13, 14, 15, 16], [17, 18, 19, 20], [21, 22, 23, 24]],
    ]
)


def test_rearrange():
    assert ARR.shape == (2, 3, 4)
    arr_out = rearrange_dimensions(ARR, 1, 2)
    expected_array = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]

    assert arr_out is not ARR
    np.testing.assert_array_equal(arr_out, expected_array)

    assert rearrange_dimensions(ARR, 2, 1).shape == (4, 3)
