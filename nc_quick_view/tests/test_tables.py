from io import BytesIO

import pytest
from netCDF4 import Dataset


@pytest.fixture
def nc_file():
    file = BytesIO()
    root_grp = Dataset("py_netcdf4.nc", "w", format="NETCDF4")
    root_grp.description = "Example simulation data"

    ndim = 128  # Size of the matrix ndim*ndim
    xdimension = 0.75
    ydimension = 0.75
    # dimensions
    root_grp.createDimension("time", None)
    root_grp.createDimension("x", ndim)
    root_grp.createDimension("y", ndim)

    # variables
    time = root_grp.createVariable("time", "f8", ("time",))
    x = root_grp.createVariable("x", "f4", ("x",))
    y = root_grp.createVariable("y", "f4", ("y",))
    field = root_grp.createVariable(
        "field",
        "f8",
        (
            "time",
            "x",
            "y",
        ),
    )
    root_grp.close()
    return file


def test_to_variable_table_content():
    pass


def test_to_variable_table_content_raise_colindex_too_large():
    pass
